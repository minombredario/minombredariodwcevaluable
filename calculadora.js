window.onload = function(){ //Acciones tras cargar la página
salida=document.getElementById("pantalla"); //elemento pantalla de salida
operaciones=document.getElementById("operaciones"); //elemento pantalla de salida
}
var Datos_Entrada = ""; //valor donde guardamos el numero
var Datos_Operaciones = ""; //aqui guardamos los datos que mostrara en la ventana de operaciones
var Acumular_Datos = "1";
var Datos_Acumular = "";
var Activar_decimal = "0";
function numero(num){
	if((Acumular_Datos == "1") && (Activar_decimal=="0")){//si acumular datos esta activado, acumula datos y los muestra en pantalla
		Datos_Acumular+=num;
		//Datos_Entrada+=num;
		Datos_Operaciones+=num;
		salida.innerHTML = Datos_Acumular;
		operaciones.innerHTML = Datos_Operaciones;

			if(num=="."){
				document.calculadora.coma.disabled=true;
				Activar_decimal="1"
			}
	}else{
		Datos_Acumular+=num;
		Datos_Entrada+=num;
		Datos_Operaciones+=num;
		salida.innerHTML = Datos_Acumular;
		operaciones.innerHTML = Datos_Operaciones;
	}

}


function borrar(){
	salida.innerHTML = 0;
	operaciones.innerHTML=0;
	Datos_Operaciones ="";
	Datos_Entrada="";
	Datos_Acumular="";
	Acumular_Datos="1";
}

function borrar_acumulado(){
	var ultimos = Datos_Acumular.length;//saco el tamaño de los datos de pantalla
	var primeros = Datos_Entrada.length;//saco el tamaño del array de datos
	var quitar = primeros-ultimos;//saco el tamaño de la cadena que ha de quedar
	if (Datos_Acumular != 0){//si datos en igual a 0...
		Datos_Entrada = Datos_Entrada.substr(0,quitar);//saco la cadena a mostrar sin los datos borrados
		Datos_Operaciones = Datos_Operaciones.substr(0,quitar);
		Datos_Acumular = "";//pongo a 0 los datos acumulados
		salida.innerHTML = 0;//muestro en pantalla el 0
		operaciones.innerHTML = Datos_Operaciones;//y en la pantalla de operaciones muestro los datos restantes
		Acumular_Datos="1";
		document.calculadora.coma.disabled=false;
		Activar_decimal="0";
	}else {
		Acumular_Datos="1";
		document.calculadora.coma.disabled=false;
		Activar_decimal="0";
	}

}

/*function cambiarSigno(){
	if(Datos_Acumular==""){

}
	Datos_Entrada="(" + eval(Datos_Entrada) + ")";
	Datos_Acumular = "-" + Datos_Acumular;
	Datos_Entrada = -1*eval(Datos_Entrada);
	salida.innerHTML = Datos_Entrada;
	Acumular_Datos="1";//reactivamos acumular
	document.calculadora.coma.disabled=false;
	Activar_decimal="0";

}
*/

function borrar_ultimo(){
	var ultimo1 = Datos_Entrada.length-1;
	var ultimo2 = Datos_Acumular.length-1;
	Datos_Entrada = Datos_Entrada.substr(0,ultimo1); //quitar el ultimo caracter
	Datos_Entrada = Datos_Entrada.substr(0,ultimo1); //quitar el ultimo caracter
	Datos_Operaciones = Datos_Operaciones.substr(0,ultimo2);
	salida.innerHTML = Datos_Acumular;
    operaciones.innerHTML = Datos_Operaciones;
}

function CambiarSigno(){

	Datos_Acumular = -1 * Datos_Acumular;
	salida.innerHTML = Datos_Acumular;
}

function operacion(entrada){
	if (Datos_Acumular<0){
		Datos_Entrada += "(" + Datos_Acumular +")";
	}else if (Datos_Acumular>0){
		Datos_Entrada += Datos_Acumular;
	}
	salida.innerHTML = 0;
	Datos_Acumular="";//reiniciamos los datos acumulados
	Acumular_Datos="0";//desactivamos la opcion de acumular
	var cal = calculos();
	cal = ajustar(cal);

	switch(entrada){
		case "sumar":

			Datos_Entrada += "+";
			Datos_Operaciones += "+";
			//cal = calculos();
			salida.innerHTML = ajustar(cal);
			Acumular_Datos="1";//reactivamos acumular
			document.calculadora.coma.disabled=false;
			Activar_decimal="0";
			break;
		case "restar":
			Datos_Entrada+= "-";
			Datos_Operaciones += "-"
			salida.innerHTML = cal;
			Acumular_Datos="1";//reactivamos acumular
			document.calculadora.coma.disabled=false;
			Activar_decimal="0";
			break;
		case "multiplicar":
			Datos_Entrada="(" + eval(Datos_Entrada) + ")";
			Datos_Operaciones += "*"
			Datos_Entrada+= "*";
			salida.innerHTML = cal;
			Acumular_Datos="1";//reactivamos acumular
			document.calculadora.coma.disabled=false;
			Activar_decimal="0";
			break;
		case "dividir":
			Datos_Entrada="(" + eval(Datos_Entrada) + ")";
			Datos_Operaciones += "/"
			Datos_Entrada+= "/";
			salida.innerHTML = cal;
			Acumular_Datos="1";//reactivamos acumular
			document.calculadora.coma.disabled=false;
			Activar_decimal="0";
			break;
		case "porcentaje":
			Datos_Entrada="(" + eval(Datos_Entrada) + ")";
			Datos_Operaciones += "%"
			Datos_Entrada+= "/100";
			salida.innerHTML = cal;
			Acumular_Datos="1";//reactivamos acumular
			document.calculadora.coma.disabled=false;
			Activar_decimal="0";
			break;
		case "cuadrado":
			Datos_Entrada="(" + eval(Datos_Entrada) + ")";
			Datos_Operaciones += "^2"
			Datos_Entrada= Math.pow(cal,2);
			salida.innerHTML = Math.pow(cal,2);
			Acumular_Datos="1";//reactivamos acumular
			document.calculadora.coma.disabled=false;
			Activar_decimal="0";
			break;
		case "raiz":
			Datos_Entrada="(" + eval(Datos_Entrada) + ")";
			Datos_Operaciones += "√2"
			Datos_Entrada= Math.pow(cal,0.5);
			salida.innerHTML = Datos_Entrada;
			Acumular_Datos="1";//reactivamos acumular
			document.calculadora.coma.disabled=false;
			Activar_decimal="0";
			break;

		case "redondeo":
			salida.innerHTML = red;
			Acumular_Datos="1";//reactivamos acumular
			document.calculadora.coma.disabled=false;
			Activar_decimal="0";
			break;


		case "igual":
			salida.innerHTML = ajustar(cal);
			operaciones.innerHTML = Datos_Entrada;
			//reiniciamos todo
			Datos_Operaciones ="";
			Datos_Entrada="";
			Datos_Acumular="";
			Acumular_Datos="1";
			Acumular_Datos="1";
			document.calculadora.coma.disabled=false;
			Activar_decimal="0";
			break;
	}
}

function calculos(){
	var resul = eval(Datos_Entrada);
	var coma =resul;
	var largo = resul.length;
	if(Datos_Entrada==0){
		salida.innerHTML = 0;
	}else {
			return resul;
		}

}

function ajustar(resultado){
	var coma = resultado.toString();
	if(coma.indexOf('.') != -1 ){
		coma = parseFloat(resultado);
		resul = coma.toFixed(13);
		return resul;
	}else {
		return resultado;
	}
}


