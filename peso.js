/*
3) Haz una página Web que mediante un formulario permita escribir un nombre, el peso en kg y la
altura en cm de una persona. Habrá un botón añadir a listado mostrado en XHTML. Si en algún
momento el peso es superior a la altura – 100cm, el nombre en el listado se mostrará en rojo. En
caso contrario, se mostrará en azul.
*/
var cont = 1;
var datos;

function obtenerDatos(evento){
	datos = new Array(); //creo una nueva array con los datos
	//guardo todos los datos en un array
	datos.push(C_Letter(document.getElementById("nombre").value));//paso el nombre por una funcion para poner la primera letra en mayusculas y el resto en minusculas
	datos.push(document.getElementById("peso").value);
	datos.push(document.getElementById("altura").value);
	for(var x = 0; x < datos.length;x++){
		if(datos[x]== ""){
			alert("Rellena todos los campos");

		//crear_tabla(); //creo las filas y la columna de los datos
		}else {crear_tabla(); //creo las filas y la columna de los datos
		break;}
	}

}
window.onload = cabecera;
function cabecera(){//cargo la cabecera de la tabla
	document.getElementById("tabla").innerHTML += "<tr style='background: grey; width: 660px;'>"
	+"<td style='width :60px;'>Número</td><td style='width : 200px;'>Nombre</td>"
	+"<td style='width : 200px;'>Peso (Kg)</td><td style='width : 200px;'>Altura (cm)</td></tr>";
	}
function crear_tabla(){

	document.getElementById("tabla").innerHTML += "<tr id="+cont+"></tr>"; //añado a la tabla una fila con el identificador de la linea que es
	document.getElementById(cont).innerHTML += "<td>" + cont+ "</td>";//pongo el numero de fila
	if(datos[1]>100){//si el peso es mayor a 100 el texto sale en rojo
		for (var x = 0; x<3;x++){//recorro el array de datos
		document.getElementById(cont).innerHTML += "<td style='width : 150px; color:red;'>" + datos[x]+ "</td>";//creo una celda con cada valor
		}
	}else{
		for (var x = 0; x<3;x++){//si el peso es menor a 100 el texto sale en azul
		document.getElementById(cont).innerHTML += "<td style='width :150px; color:blue'>" + datos[x]+ "</td>";
		}
	}
	if (document.getElementById(cont).id%2==0){
		document.getElementById(cont).style.background = "#dcdcdc"
	}


	cont++;//sumo 1 al contador de lineas

}
function C_Letter(a){//funcion para conseguir la capital letter
	b = a.charAt(0).toUpperCase();
	for (var i = 1; i < a.length;i++){
		b +=a[i].toLowerCase();
	}
	return b;
}
