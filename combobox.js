/*
1) Selecciona 4 equipos de fútbol y 5 jugadores de cada equipo (si no os gusta el futbol, pueden ser
inventados).
Crea dos combobox, una para equipos y otra para jugadores. La de equipos contendrá los equipos
y tendrá como texto por defecto “Elige tu equipo” y la de jugadores inicialmente estará vacía.
Cada vez que se realice un cambio en el combobox del equipo, se actualizará el listado de
jugadores.
*/

var array_equipos = new Array ("Seleccione un equipo","Madrid", "Barcelona", "Atletico","Sevilla");
//real madrid
var equipo1 = new Array ("1 KEYLOR NAVAS" ,"13 K. CASILLA ", "25 RUBÉN YÁÑEZ", "2 CARVAJAL",
			"3 PEPE", "4 SERGIO RAMOS", "5 VARANE", "6 NACHO", "12 MARCELO",
			"15 COENTRÃO", "23 DANILO", "8 KROOS", "10 JAMES RODRÍGUEZ",
			"14 CASEMIRO", "16 KOVACIC", "19 MODRIC", "20 ASENSIO", "22 ISCO",
			"7 RONALDO", "9 BENZEMA", "11 BALE","17 LUCAS VÁZQUEZ","18 MARIANO","21 MORATA");
//barcelona
var equipo2 = new Array ("0 Samuel Umtiti","0 Lucas Digne","0 André Gomes","0 Denis Suárez",
			"1 Marc-André Ter Stegen","2 Douglas Pereira","3 Gerard Piqué","4 Ivan Rakitic",
			"5 Sergio Busquets","7 Arda Turan","8 Andrés Iniesta","9 Luís Suárez","10 Leo Messi",
			"11 Neymar Jr.","12 Rafa Alcántara ","13 Carlos Bravo","14 Javier Mascherano",
			"17 Munir El Haddadi","19 Sandro Ramírez","20 Sergi Roberto","22 Aleix Vidal",
			"23 Thomas Vermaelen","24 Jeremy Mathieu","25 Jordi Masip","26 Sergi Samper",
			"27 Juan Cámara","28 Gerard Gumbau","34 Wilfrid Kaptoum","35 Adrián Ortolá");
//atletico
var equipo3 = new Array("1 Moyá","2 Diego Godín","3 Filipe Luís","5 Tiago","6 Koke",
			"7 Antoine Griezmann","8 Matías Kranevitter","9 Fernando Torres","9 Luciano Vietto",
			"10 Óliver Torres","12 Augusto Fernández","13 Jan Oblak","14 Gabi","15 Stefan Savić",
			"16 Ángel Correa","17 Saúl","18 Jesús Gámez","19 Lucas Hernández","20 Juanfran",
			"21 Yannick Ferreira-Carrasco","22 Thomas Partey","23 Luciano Vietto","24 José Giménez",
			"27 Borja González","28 Roberto Núñez","32 Nacho Monsalve","33 Theo Hernández",
			"34 Samuel Villa");

//sevilla
var equipo4 = new Array("1 Sergio Rico","2 Benoît Trémoulinas","3 Mariano","4 Matías Kranevitter",
			"5 Timothée Kolodziejczak","6 Daniel Carriço","7 Michael Krohn-Dehli","8 Iborra",
			"9 Luciano Vietto","10 Samir Nasri","11 Joaquín Correa","12 Wissam Ben Yedder",
			"13 David Soria","14 Hiroshi Kiyotake","15 Steven N","Zonzi","17 Pablo Sarabia",
			"18 Escudero","19 Ganso","20 Vitolo","21 Nicolás Pareja","22 Franco Vázquez","23 Adil Rami",
			"24 Gabriel Mercado","25 Salvatore Sirigu","27 Cotán","30 Carlos Fernández","31 Churripi",
			"32 Diego González","36 Álex Muñoz");

var array_jugadores = new Array(equipo1,equipo2,equipo3,equipo4);
var todos = new Array();


//cargar los elementos en el primero combobox
window.onload = N_equipos;//creo una llamada a la función N_equipos para cargar los valores del primer combobox

function N_equipos(){
	var listado = document.getElementById("Listado_equipos");//creo una variable con la llamada, para no tener que llamar a cada rato a getElementById
	for (var x = 0; x < array_equipos.length;x++ ){//recorro el array de los equipos
		listado.options[x] = new Option(array_equipos[x]);//asigno una posicion y un valor a cada una de las posiciones del array
		listado.options[x].value = array_equipos[x];//asigno un value con el nombre del equipo para la llamada de carga de los jugadores

	}

//creo un array bidimensional con los equipos y sus jugadores
	for (var x=1, y=0;x<array_equipos.length;x++,y++){
		todos.push(array_equipos[x],array_jugadores[y])
	}
}

function Equipos(evento){
	document.body.style.backgroundRepeat='no-repeat';
	document.body.style.backgroundPosition ="50% -50%";
	document.body.style.backgroundSize  = "600px 600px";
	document.getElementById("Listado_jugadores").options.length=0;//vaciamos Listado_jugadores
	switch(evento){
		case "Madrid":
			rellenar(evento);
			document.body.style.backgroundImage='url(img/madrid.gif)';
			break;
		case "Barcelona":
			rellenar(evento);
			document.body.style.backgroundImage='url(img/barcelona.jpg)';
			break;
		case "Atletico":
			rellenar(evento);
			document.body.style.backgroundImage='url(img/atletico.jpg)';
			break;
		case "Sevilla":
			rellenar(evento);
			document.body.style.backgroundImage='url(img/sevilla.jpg)';
			break;
	}
}

function rellenar(evento){

	var listado = document.getElementById("Listado_jugadores");//creo una variable con la llamada, para no tener que llamar a cada rato a getElementById
	var cont =0;
	for (var x = 0; x < todos.length;x++ ){//recorro el array con todos los datos
			if(evento==todos[x]){//si el valor del evento (equipo de futbol) coincide, entonces..
				for(var y = 0; y < todos[x+1].length;y++){//recorro el array de sus jugadores y...
					listado.options[cont] = new Option(todos[x+1][y]);//los voy añadiendo al combobox
					listado.options[cont].value = todos[x];//asigno un value con el nombre del equipo para la llamada de carga de los jugadores
					cont++;//sumamos uno al contador que sera la asignacion de la posicion de nuestro jugador en el combo
				}break;
			}
	}

}
