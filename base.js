/*
2) Haz una página Web que mediante un formulario permita escribir un número, una base origen y
una base destino. Habrá un botón convertir base. Si el número no esta en la base origen, saltará un
mensaje de error. En caso contrario, el programa mostrará en algún elemento XHTML el resultado
de la conversión del número a la base destino.
*/
var valor;
var B_origen;
var B_destino;

function obtenerDatos(datos){
	valor = document.getElementById("valor").value.toUpperCase();
	B_origen = document.getElementById("origen").value;
	B_destino = document.getElementById("destino").value;
	document.getElementById("salida").innerHTML="";
	calcular(valor,B_origen,B_destino);
}


function calcular(v,bo,bd){
	var origen = parseInt(v,bo);
	var destino = origen.toString(bd).toUpperCase();
	var comprobar = parseInt(destino,bd);
	if(origen == comprobar || origen == destino){
		document.getElementById("salida").innerHTML+= "El Valor " + v + bo.sub() + " = " + destino + bd.sub();
		limpiar();
	}else {
		alert("La base origen es incorrecta");
	}

}

function limpiar(){
	document.form.valor.value="";;
	document.getElementById("origen").value='';
	document.getElementById("destino").value='';
	document.getElementById("salida").style.color = "blue";
}
